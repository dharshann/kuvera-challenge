class Scheme < ApplicationRecord
  has_many :statistics, dependent: :destroy
end
