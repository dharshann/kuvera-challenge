class AbsoluteReturnsService
  include SchemesHelper

  def initialize(params)
    @scheme = params[:scheme]
    @invested_date = Date.parse(params[:invested_date])
    @invested_amount = params[:invested_amount].to_f
  end

  def call
    total_profit_amount.round(2)
  end

  private

  def total_profit_amount
    @invested_amount + profit_amount
  end

  def absolute_returns_percent
    current_day_nav = SchemesHelper.nav_of_the_day(@scheme, Date.today, 'current')
    invested_day_nav = SchemesHelper.nav_of_the_day(@scheme, @invested_date)
    (current_day_nav - invested_day_nav) / invested_day_nav * 100
  end

  def profit_amount
    (absolute_returns_percent / 100) * @invested_amount
  end
end
