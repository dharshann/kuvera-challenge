# README
Instructions:

* clone the repo

* `bundle install` install required gems

* Project uses `PostgreSQL` as database. Refere [here](https://www.digitalocean.com/community/tutorials/how-to-install-and-use-postgresql-on-ubuntu-16-04) for installing.

 ~~Change `download_nav_data.rb` if you need more up-to-date nav data from the provider~~
 

* export your database credentials into `PGUSER` and `PGPASS` ENV variables

* `rails db:setup` to setup required databases and data

* Fire up the server `rails s`

* Visit `localhost:3000`

* For CAGR use GET `localhost:3000/schemes/cagr_returns` route by passing `{ scheme_id: X,  invested_date: 'xxxx-yy-zz', invested_amount: XYZQ}`

* For XIRR use POST `localhost:3000/schemes/xirr_returns` route by passing `json` payload as bellow
  ```
  {
	"scheme": {
		"xirr": [	
			{
				"transaction_date": "2018-1-1",
				"amount": "10000"
			},
			{
				"transaction_date": "2018-2-1",
				"amount": "-5000"
			},
			{
				"transaction_date": "2018-3-1",
				"amount": "7000"
			}
			]
		}
}
```