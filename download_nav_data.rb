require 'open-uri'
require 'csv'

content_array = []
open('http://portal.amfiindia.com/DownloadNAVHistoryReport_Po.aspx?mf=53&tp=1&frmdt=01-Apr-2015&todt=25-Aug-2018') do |f|
  f.each_line do |line|
    next if line == "\r\n"
    line = line.delete("\r\n")
    content_array << line
  end
end
content_array.shift(3)
content_array.map! { |a| a.split(';') }

File.write('nav_data.csv', content_array.map(&:to_csv).join)
puts "imported #{content_array.count} values"