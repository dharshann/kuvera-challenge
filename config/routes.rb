Rails.application.routes.draw do
  root 'schemes#index'

  get 'schemes/index'
  get 'schemes/absolute_returns'
  get 'schemes/cagr_returns'
  post 'schemes/xirr_returns', to: 'schemes#xirr_returns'
end
