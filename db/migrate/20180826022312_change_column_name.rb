class ChangeColumnName < ActiveRecord::Migration[5.1]
  def change
    rename_column :schemes, :scheme_code, :code
    rename_column :schemes, :scheme_name, :name
  end
end
