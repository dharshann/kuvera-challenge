class CreateStatistics < ActiveRecord::Migration[5.1]
  def change
    create_table :statistics do |t|
      t.float :nav
      t.float :repurchase_price
      t.float :sale_price
      t.date :nav_date
      t.references :scheme, foreign_key: true

      t.timestamps
    end
  end
end
